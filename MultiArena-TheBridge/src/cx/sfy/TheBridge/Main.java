package cx.sfy.TheBridge;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import cx.sfy.TheBridge.cmds.SetupCMD;
import cx.sfy.TheBridge.controllers.WorldController;
import cx.sfy.TheBridge.database.Cosmeticbase;
import cx.sfy.TheBridge.database.Database;
import cx.sfy.TheBridge.database.PlayerStat;
import cx.sfy.TheBridge.game.GameDuo;
import cx.sfy.TheBridge.game.GameDuo.State;
import cx.sfy.TheBridge.game.GameFour;
import cx.sfy.TheBridge.game.GameFour.FState;
import cx.sfy.TheBridge.game.InventoryData;
import cx.sfy.TheBridge.game.PlayerData;
import cx.sfy.TheBridge.hooks.PlaceholderHook;
import cx.sfy.TheBridge.kit.Hotbar;
import cx.sfy.TheBridge.kit.Kit;
import cx.sfy.TheBridge.leaderheads.TheBridgeFourGoals;
import cx.sfy.TheBridge.leaderheads.TheBridgeFourKills;
import cx.sfy.TheBridge.leaderheads.TheBridgeFourWins;
import cx.sfy.TheBridge.leaderheads.TheBridgeNormalGoals;
import cx.sfy.TheBridge.leaderheads.TheBridgeNormalKills;
import cx.sfy.TheBridge.leaderheads.TheBridgeNormalWins;
import cx.sfy.TheBridge.listeners.PlayerListener;
import cx.sfy.TheBridge.listeners.SetupListener;
import cx.sfy.TheBridge.listeners.SpectatorListener;
import cx.sfy.TheBridge.managers.ArchiManager;
import cx.sfy.TheBridge.managers.CageManager;
import cx.sfy.TheBridge.managers.FileManager;
import cx.sfy.TheBridge.managers.GameManager;
import cx.sfy.TheBridge.managers.GlassManager;
import cx.sfy.TheBridge.managers.LocationManager;
import cx.sfy.TheBridge.managers.ParticleManager;
import cx.sfy.TheBridge.managers.ScoreboardManager;
import cx.sfy.TheBridge.managers.SetupManager;
import cx.sfy.TheBridge.managers.SignManager;
import cx.sfy.TheBridge.managers.TitleManager;
import cx.sfy.TheBridge.managers.TopManager;
import cx.sfy.TheBridge.menus.AchievementsMenu;
import cx.sfy.TheBridge.menus.GameMenu;
import cx.sfy.TheBridge.menus.GlassMenu;
import cx.sfy.TheBridge.menus.ShopMenu;
import cx.sfy.TheBridge.menus.SpectOptionsMenu;
import cx.sfy.TheBridge.menus.SpectPlayerMenu;
import cx.sfy.TheBridge.menus.TeamMenu;
import cx.sfy.TheBridge.menus.particle.ArrowTrailMenu;
import cx.sfy.TheBridge.menus.particle.FeetTrailMenu;
import cx.sfy.TheBridge.menus.particle.TrailMenu;
import cx.sfy.TheBridge.nms.GenericNMS;
import cx.sfy.TheBridge.nms.NMS;
import cx.sfy.TheBridge.packets.PacketMain;
import cx.sfy.TheBridge.packets.ParticleHandler;
import lombok.Getter;
import lombok.Setter;

public class Main extends JavaPlugin {

	private static Main instance;
	@Getter
	@Setter
	private PacketMain refl;
	@Getter
	private SpectOptionsMenu som;
	@Getter
	private SpectPlayerMenu spm;
	@Getter
	private TeamMenu tem;
	@Getter
	private boolean placeholder;
	@Getter
	private TitleManager tm;
	@Getter
	private GameManager gm;
	@Getter
	private GameMenu gmu;
	@Getter
	private TrailMenu trm;
	@Getter
	private ArrowTrailMenu atm;
	@Getter
	private FeetTrailMenu ftm;
	@Getter
	private WorldController wc;
	@Getter
	private Settings lang;
	@Getter
	private Settings cages;
	@Getter
	private Settings signs;
	@Getter
	private Settings sounds;
	@Getter
	private Settings achievement;
	@Getter
	private Settings particles;
	@Getter
	private NMS nms;
	@Getter
	private FileManager fm;
	@Getter
	private SetupManager sm;
	@Getter
	private GlassManager glm;
	@Getter
	private LocationManager lm;
	@Getter
	private Location mainLobby;
	@Getter
	private Kit kit;
	@Getter
	private Database db;
	@Getter
	private TopManager top;
	@Getter
	private SignManager sim;
	@Getter
	private Hotbar hotbar;
	@Getter
	private boolean stop;
	@Getter
	private ScoreboardManager sb;
	@Getter
	private ArchiManager am;
	@Getter
	private AchievementsMenu archimenu;
	@Getter
	private CageManager cm;
	@Getter
	private ParticleManager pam;
	@Getter
	private GlassMenu glam;
	@Getter
	private boolean isCage = false;
	@Getter
	private boolean archiDisabled = false;
	@Getter
	private ShopMenu shop;
	@Getter
	private Cosmeticbase cb;

	public static Main get() {
		return instance;
	}

	@Override
	public void onEnable() {
		refl = new PacketMain(this);
		ParticleHandler.load();
		instance = this;
		stop = false;
		getServer().getMessenger().registerOutgoingPluginChannel(Main.get(), "BungeeCord");
		getConfig().options().copyDefaults(true);
		saveConfig();
		File s = new File(getDataFolder() + "/cages");
		if (!s.exists()) {
			s.mkdirs();
		}
		saveResources();
		lang = new Settings(this, "lang");
		signs = new Settings(this, "signs");
		cages = new Settings(this, "cages");
		sounds = new Settings(this, "sounds");
		achievement = new Settings(this, "achievement");
		particles = new Settings(this, "particles");
		if (getServer().getPluginManager().isPluginEnabled("FastAsyncWorldEdit")) {
			if (!getConfig().getBoolean("cages")) {
				Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "You have " + ChatColor.RED
						+ "FAWE installed." + ChatColor.GREEN + "Cage System Enabled");
			} else {
				cm = new CageManager(this);
				isCage = true;
			}
		} else {
			if (getConfig().getBoolean("cages")) {
				Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "You don't have " + ChatColor.YELLOW
						+ "FAWE installed." + ChatColor.RED + "Cage System Disabled");
				isCage = false;
			}
		}
		archiDisabled = getConfig().getBoolean("archievements.disable");
		if (getServer().getPluginManager().isPluginEnabled("PlaceholderAPI")) {
			placeholder = true;
			new PlaceholderHook(this).register();
		}
		loadNMS();
		File a = new File(getDataFolder() + "/arenas");
		if (!a.exists()) {
			a.mkdirs();
		}
		File a1 = new File(getDataFolder() + "/maps");
		if (!a1.exists()) {
			a1.mkdirs();
		}
		db = new Database(this);
		cb = new Cosmeticbase(this);
		kit = new Kit();
		lm = new LocationManager(this);
		fm = new FileManager(this);
		wc = new WorldController(this);
		tm = new TitleManager(this);
		sim = new SignManager(this);
		gm = new GameManager(this);
		gmu = new GameMenu(this);
		trm = new TrailMenu(this);
		atm = new ArrowTrailMenu(this);
		ftm = new FeetTrailMenu(this);
		sm = new SetupManager(this);
		glm = new GlassManager();
		pam = new ParticleManager(this);
		top = new TopManager(this);
		tem = new TeamMenu(this);
		som = new SpectOptionsMenu(this);
		spm = new SpectPlayerMenu(this);
		hotbar = new Hotbar(this);
		sb = new ScoreboardManager(this);
		am = new ArchiManager(this);
		archimenu = new AchievementsMenu(this);
		glam = new GlassMenu(this);
		shop = new ShopMenu(this);

		lm.reloadLocations();
		top.updateTops();
		if (getConfig().getString("mainLobby") == null) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "The main lobby hasn't been set up. Please use "
					+ ChatColor.YELLOW + "/bridges setmainlobby");
		} else {
			mainLobby = getStringLocation(getConfig().getString("mainLobby"));
		}
		getCommand("bridges").setExecutor(new SetupCMD(this));
		Bukkit.getServer().getPluginManager().registerEvents(new SetupListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new SpectatorListener(this), this);
		new BukkitRunnable() {
			@Override
			public void run() {
				top.updateTops();
			}
		}.runTaskTimer(this, 6000, 6000);
		new BukkitRunnable() {
			@Override
			public void run() {
				for (GameDuo game : getGm().getGames()) {
					if (game.isState(State.WAITING) || game.isState(State.STARTING) || game.isState(State.PREGAME)) {
						game.updateGame();
					}
				}
				for (GameFour game : getGm().getGamesFour()) {
					if (game.isState(FState.WAITING) || game.isState(FState.STARTING) || game.isState(FState.PREGAME)) {
						game.updateGame();
					}
				}
			}
		}.runTaskTimer(this, 20, 20);
		if (getServer().getPluginManager().isPluginEnabled("LeaderHeads")) {
			new TheBridgeNormalKills();
			new TheBridgeNormalWins();
			new TheBridgeNormalGoals();
			new TheBridgeFourKills();
			new TheBridgeFourWins();
			new TheBridgeFourGoals();
		}
	}

	@Override
	public void onDisable() {
		stop = true;
		getTop().removeHolo();
		if (InventoryData.getInventoryData() != null || !InventoryData.getInventoryData().values().isEmpty()
				|| !InventoryData.getInventoryData().isEmpty()) {
			for (InventoryData inv : InventoryData.getInventoryData().values()) {
				inv.restore();
			}
		}
		for (GameFour gamef : getGm().getGamesFour()) {
			for (PlayerData pd : gamef.getPD().values()) {
				pd.restore();
			}
		}
		for (GameDuo gamef : getGm().getGames()) {
			for (PlayerData pd : gamef.getPD().values()) {
				pd.restore();
			}
		}
		for (Player on : Bukkit.getOnlinePlayers()) {
			getDb().saveData(PlayerStat.getPlayerStat(on));
			getCb().saveData(PlayerStat.getPlayerStat(on));
		}
		getServer().getScheduler().cancelTasks(this);
	}

	public void loadNMS() {
		nms = new GenericNMS();
	}

	public void saveResources() {
		saveResource("clear.schematic", false);
		saveResource("normal-red.schematic", false);
		saveResource("normal-blue.schematic", false);
		saveResource("normal-green.schematic", false);
		saveResource("normal-yellow.schematic", false);
		File cage = new File(getDataFolder(), "cages");
		File c = new File(getDataFolder(), "clear.schematic");
		File r = new File(getDataFolder(), "normal-red.schematic");
		File b = new File(getDataFolder(), "normal-blue.schematic");
		File g = new File(getDataFolder(), "normal-green.schematic");
		File y = new File(getDataFolder(), "normal-yellow.schematic");
		copyFiles(c, new File(cage, "clear.schematic"));
		copyFiles(r, new File(cage, "normal-red.schematic"));
		copyFiles(b, new File(cage, "normal-blue.schematic"));
		copyFiles(g, new File(cage, "normal-green.schematic"));
		copyFiles(y, new File(cage, "normal-yellow.schematic"));
		c.delete();
		r.delete();
		g.delete();
		b.delete();
		y.delete();
	}

	public void copyFiles(File file, File file2) {
		try {
			if (!new ArrayList<String>(Arrays.asList("uid.dat", "session.dat")).contains(file.getName())) {
				if (file.isDirectory()) {
					if (!file2.exists()) {
						file2.mkdirs();
					}
					String[] list;
					for (int length = (list = file.list()).length, i = 0; i < length; ++i) {
						String s = list[i];
						copyFiles(new File(file, s), new File(file2, s));
					}
				} else {
					FileOutputStream fileOutputStream;
					try (FileInputStream fileInputStream = new FileInputStream(file)) {
						fileOutputStream = new FileOutputStream(file2);
						byte[] array = new byte[1024];
						int read;
						while ((read = fileInputStream.read(array)) > 0) {
							fileOutputStream.write(array, 0, read);
						}
					}
					fileOutputStream.close();
				}
			}
		} catch (IOException ex) {
		}
	}

	public void reloadMainLobby() {
		mainLobby = getStringLocation(getConfig().getString("mainLobby"));
	}

	public Location getStringLocation(String location) {
		String[] l = location.split(";");
		World world = Bukkit.getWorld(l[0]);
		double x = Double.parseDouble(l[1]);
		double y = Double.parseDouble(l[2]);
		double z = Double.parseDouble(l[3]);
		float yaw = Float.parseFloat(l[4]);
		float pitch = Float.parseFloat(l[5]);
		return new Location(world, x, y, z, yaw, pitch);
	}

}