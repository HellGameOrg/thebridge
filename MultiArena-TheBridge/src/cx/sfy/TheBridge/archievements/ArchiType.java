package cx.sfy.TheBridge.archievements;

public enum ArchiType {

	BLOCKS_BROKEN,
	BLOCKS_PLACED,
	KILLS,
	GOALS,
	WINS;
	
}