package cx.sfy.TheBridge.hologram;

import java.util.logging.Level;

import org.bukkit.Bukkit;

import cx.sfy.TheBridge.hologram.TruenoHologram_v1_10_R1;
import cx.sfy.TheBridge.hologram.TruenoHologram_v1_11_R1;
import cx.sfy.TheBridge.hologram.TruenoHologram_v1_8_R3;
import cx.sfy.TheBridge.hologram.TruenoHologram_v1_9_R2;
import net.md_5.bungee.api.ChatColor;

public class TruenoHologramAPI {

	private static String version;

	private static void setupVersion() {
		try {
			version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
		} catch (ArrayIndexOutOfBoundsException ex) {
			ex.printStackTrace();
		}
	}

	public static TruenoHologram getNewHologram() {
		if (version == null) {
			setupVersion();
		}
		if (version.equals("v1_8_R3")) {
			return new TruenoHologram_v1_8_R3();
		} else if (version.equals("v1_9_R2")) {
			return new TruenoHologram_v1_9_R2();
		} else if (version.equals("v1_10_R1")) {
			return new TruenoHologram_v1_10_R1();
		} else if (version.equals("v1_11_R1")) {
			return new TruenoHologram_v1_11_R1();
		} else if (version.equals("v1_12_R1")) {
			return new TruenoHologram_v1_12_R1();
		} else if (version.equals("v1_13_R2")) {
			return new TruenoHologram_v1_13_R2();
		}else if (version.equals("v1_14_R1")) {
			return new TruenoHologram_v1_14_R1();
		}
		else {
			Bukkit.getLogger().log(Level.SEVERE, ChatColor.RED + "Unsopported server version.");
			return null;
		}

	}

}
