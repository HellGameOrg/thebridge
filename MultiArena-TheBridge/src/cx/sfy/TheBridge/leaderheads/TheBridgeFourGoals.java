package cx.sfy.TheBridge.leaderheads;

import java.util.Arrays;

import org.bukkit.entity.Player;

import cx.sfy.TheBridge.database.PlayerStat;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;

public class TheBridgeFourGoals extends OnlineDataCollector {

    public TheBridgeFourGoals() {
        super("bridges-f-goals", "TheBridge", BoardType.DEFAULT, "&bBridges Four - Kills", "bridgesfgoals", Arrays.asList("&c----------", "&a{name}", "&e{amount} goals", "&c----------"));
    }
    
    @Override
	public Double getScore(Player p) {
		return (double)PlayerStat.getPlayerStat(p).getFourGoals();
	}
    
}