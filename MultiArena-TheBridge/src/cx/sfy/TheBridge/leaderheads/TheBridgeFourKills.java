package cx.sfy.TheBridge.leaderheads;

import java.util.Arrays;

import org.bukkit.entity.Player;

import cx.sfy.TheBridge.database.PlayerStat;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;

public class TheBridgeFourKills extends OnlineDataCollector {

    public TheBridgeFourKills() {
        super("bridges-f-kills", "TheBridge", BoardType.DEFAULT, "&bBridges Four - Kills", "bridgesfkills", Arrays.asList("&c----------", "&a{name}", "&e{amount} kills", "&c----------"));
    }
    
    @Override
	public Double getScore(Player p) {
		return (double)PlayerStat.getPlayerStat(p).getFourKills();
	}

}