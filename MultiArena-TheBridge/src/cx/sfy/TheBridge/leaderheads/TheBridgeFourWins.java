package cx.sfy.TheBridge.leaderheads;

import java.util.Arrays;

import org.bukkit.entity.Player;

import cx.sfy.TheBridge.database.PlayerStat;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;

public class TheBridgeFourWins extends OnlineDataCollector {

    public TheBridgeFourWins() {
        super("bridges-f-wins", "TheBridge", BoardType.DEFAULT, "&bBridges Four - Wins", "bridgesfwins", Arrays.asList("&c----------", "&a{name}", "&e{amount} wins", "&c----------"));
    }
    
    @Override
	public Double getScore(Player p) {
		return (double)PlayerStat.getPlayerStat(p).getFourWins();
	}
    
}