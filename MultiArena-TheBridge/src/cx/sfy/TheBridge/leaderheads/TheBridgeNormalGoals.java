package cx.sfy.TheBridge.leaderheads;

import java.util.Arrays;

import org.bukkit.entity.Player;

import cx.sfy.TheBridge.database.PlayerStat;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;

public class TheBridgeNormalGoals extends OnlineDataCollector {

    public TheBridgeNormalGoals() {
        super("bridges-n-goals", "TheBridge", BoardType.DEFAULT, "&bBridges Normal - Goals", "bridgesngoals", Arrays.asList("&c----------", "&a{name}", "&e{amount} goals", "&c----------"));
    }
    
    @Override
	public Double getScore(Player p) {
		return (double)PlayerStat.getPlayerStat(p).getNormalKills();
	}
	
}