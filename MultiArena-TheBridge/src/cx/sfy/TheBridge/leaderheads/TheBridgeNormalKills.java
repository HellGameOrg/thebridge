package cx.sfy.TheBridge.leaderheads;

import java.util.Arrays;

import org.bukkit.entity.Player;

import cx.sfy.TheBridge.database.PlayerStat;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;

public class TheBridgeNormalKills extends OnlineDataCollector {

	public TheBridgeNormalKills() {
        super("bridges-n-kills", "TheBridge", BoardType.DEFAULT, "&bBridges Normal - Kills", "bridgesnkills", Arrays.asList("&c----------", "&a{name}", "&e{amount} kills", "&c----------"));
    }

	@Override
	public Double getScore(Player p) {
		return (double)PlayerStat.getPlayerStat(p).getNormalKills();
	}
    
}