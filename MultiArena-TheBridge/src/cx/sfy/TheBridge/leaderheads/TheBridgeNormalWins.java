package cx.sfy.TheBridge.leaderheads;

import java.util.Arrays;

import org.bukkit.entity.Player;

import cx.sfy.TheBridge.database.PlayerStat;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;

public class TheBridgeNormalWins extends OnlineDataCollector {
 
    public TheBridgeNormalWins() {
        super("bridges-n-wins", "TheBridge", BoardType.DEFAULT, "&bBridges Normal - Wins", "bridgesnwins", Arrays.asList("&c----------", "&a{name}", "&e{amount} wins", "&c----------"));
    }
    
    @Override
	public Double getScore(Player p) {
		return (double)PlayerStat.getPlayerStat(p).getNormalWins();
	}
    
}