package cx.sfy.TheBridge.menus;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import cx.sfy.TheBridge.Main;
import cx.sfy.TheBridge.cosmetics.Cage;

public class GlassMenu {

	Main plugin;

	public GlassMenu(Main plugin) {
		this.plugin = plugin;
	}

	public void createGlassMenu(Player p) {
		Inventory inv = Bukkit.getServer().createInventory(null, plugin.getCages().getInt("size"),
				plugin.getCages().get("title"));
		for (Cage cage : plugin.getCm().getCages().values()) {
			if (!cage.isBuy() && !p.hasPermission(cage.getPermission())) {
				inv.setItem(cage.getSlot(), cage.getPermIcon());
			} else if (cage.isBuy() && !p.hasPermission(cage.getPermission())) {
				inv.setItem(cage.getSlot(), cage.getBuyIcon());
			} else {
				inv.setItem(cage.getSlot(), cage.getHasIcon());
			}
		}
		p.openInventory(inv);
	}

}