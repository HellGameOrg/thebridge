package cx.sfy.TheBridge.menus.particle;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import cx.sfy.TheBridge.Main;
import cx.sfy.TheBridge.cosmetics.Particle;

public class ArrowTrailMenu {
	Main plugin;

	public ArrowTrailMenu(Main plugin) {
		this.plugin = plugin;
	}

	public void createArrowMenu(Player p) {
		Inventory inv = Bukkit.getServer().createInventory(null, plugin.getParticles().getInt("trails.arrow.size"),
				plugin.getParticles().get("trails.arrow.title"));
		for (Particle arrow : plugin.getPam().getArrow_trails().values()) {
			if (!arrow.isBuy() && !p.hasPermission(arrow.getPermission())) {
				inv.setItem(arrow.getSlot(), arrow.getPermIcon());
			} else if (arrow.isBuy() && !p.hasPermission(arrow.getPermission())) {
				inv.setItem(arrow.getSlot(), arrow.getBuyIcon());
			} else {
				inv.setItem(arrow.getSlot(), arrow.getHasIcon());
			}
		}
		p.openInventory(inv);
	}

}
