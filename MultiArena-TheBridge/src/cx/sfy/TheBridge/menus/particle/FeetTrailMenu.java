package cx.sfy.TheBridge.menus.particle;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import cx.sfy.TheBridge.Main;
import cx.sfy.TheBridge.cosmetics.Particle;

public class FeetTrailMenu {
	Main plugin;

	public FeetTrailMenu(Main plugin) {
		this.plugin = plugin;
	}

	public void createFeetMenu(Player p) {
		Inventory inv = Bukkit.getServer().createInventory(null, plugin.getParticles().getInt("trails.feet.size"),
				plugin.getParticles().get("trails.feet.title"));
		for (Particle feet : plugin.getPam().getFeet_trails().values()) {
			if (!feet.isBuy() && !p.hasPermission(feet.getPermission())) {
				inv.setItem(feet.getSlot(), feet.getPermIcon());
			} else if (feet.isBuy() && !p.hasPermission(feet.getPermission())) {
				inv.setItem(feet.getSlot(), feet.getBuyIcon());
			} else {
				inv.setItem(feet.getSlot(), feet.getHasIcon());
			}
		}
		p.openInventory(inv);
	}

}
