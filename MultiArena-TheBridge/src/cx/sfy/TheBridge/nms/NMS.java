package cx.sfy.TheBridge.nms;

import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;

import cx.sfy.TheBridge.packets.ParticleHandler;
import cx.sfy.TheBridge.packets.ParticleHandler.ParticleType;

public interface NMS {

	ChunkGenerator getChunkGenerator();

	void sendTitle(Player player, int fadein, int stay, int fadeout, String title, String subtitle);

	void sendActionBar(Player player, String s);

	ParticleHandler sendParticle(ParticleType type, double speed, int count, double radius);

}