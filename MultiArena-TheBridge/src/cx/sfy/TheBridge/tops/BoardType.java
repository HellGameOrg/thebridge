package cx.sfy.TheBridge.tops;

public enum BoardType {

	NORMAL_KILLS,
	NORMAL_WINS,
	NORMAL_GOALS,
	FOUR_KILLS,
	FOUR_WINS,
	FOUR_GOALS;
	
}
