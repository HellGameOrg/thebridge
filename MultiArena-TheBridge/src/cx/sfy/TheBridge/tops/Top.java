package cx.sfy.TheBridge.tops;

import cx.sfy.TheBridge.Main;

public class Top {

	private BoardType type;
	private int amount;
	private int top;
	private String name;

	public Top(String name, int amount, BoardType type) {
		this.type = type;
		this.amount = amount;
		this.name = name;
		this.top = Main.get().getTop().getTopNumber(this);
		Main.get().getTop().addTop(this);
	}

	public BoardType getType() {
		return type;
	}

	public int getTop() {
		return top;
	}

	public int getAmount() {
		return amount;
	}

	public String getName() {
		return name;
	}

}