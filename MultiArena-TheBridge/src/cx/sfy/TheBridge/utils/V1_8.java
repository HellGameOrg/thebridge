package cx.sfy.TheBridge.utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class V1_8 {

	public static void sendActionbar(Player player, String s) {
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + s + "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
		CraftPlayer cfp = (CraftPlayer) player;
		cfp.getHandle().playerConnection.sendPacket(ppoc);
	}

}
